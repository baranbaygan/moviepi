#!/bin/sh
xset -dpms     # disable DPMS (Energy Star) features.
xset s off       # disable screen saver
xset s noblank # don't blank the video device
matchbox-window-manager &
kweb3 -KJE  http://localhost:9615
