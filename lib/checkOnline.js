var online = false;

function checkInternet(cb) {

  require('dns').lookup('google.com', function(err) {
    if (err && err.code == "ENOTFOUND") {
      if (online)
        cb(false);

      online = false;
    } else {
      if (!online)
        cb(true);

      online = true;
    }

    setTimeout(function() {
      checkInternet(cb);
    }, 5000);
  })
}

module.exports = checkInternet;