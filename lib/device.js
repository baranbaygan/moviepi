var storage = require('node-persist');
var Firebase = require('firebase');
var logger = require('./logger');
var events = require('events');
var Wireless = require('wireless');
var spawn = require('child_process').spawn;


var device = null;
var wireless = null;

storage.initSync();

function makeid(count) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (var i = 0; i < count; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


function buildModule() {

  if (device) return device;



  device = new events.EventEmitter();


  device.wireless = new Wireless({
    iface: 'wlan0',
    updateFrequency: 10, // Optional, seconds to scan for networks
    connectionSpyFrequency: 2, // Optional, seconds to scan if connected
    vanishThreshold: 2 // Optional, how many scans before network considered gone
  });


  device.createDeviceInfo = function() {
    deviceInfo = {
      id: makeid(7)
    }
    storage.setItem('deviceInfo', deviceInfo);
    return deviceInfo;
  }
  device.initDevice = function() {

    //storage.clear();

    var deviceInfo = storage.getItem('deviceInfo');
    if (deviceInfo == null) {
      this.createDeviceInfo();
    }
  }

  device.getDeviceInfo = function() {
    var deviceInfo = storage.getItem('deviceInfo');
    if (deviceInfo == null) {
      return this.createDeviceInfo();
    }
    return deviceInfo;
  }
  device.stopScanWireless = function() {
    if (!device.wireless) return;
    device.wireless.stop(function() {})
  }
  device.scanWireless = function() {
    if (!device.wireless) return;

    function emitWirelessList(list) {

      var arr = [];
      var obj = device.wireless.list();
      for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
          arr.push(obj[key]);
        }
      };

      device.emit('wirelessList', arr);
    }

    device.wireless.enable(function(err) {

      logger.error('Wireless enable error: ' + err);

      device.wireless.start();

      device.wireless.on('appear', function() {
        emitWirelessList();
      });
      device.wireless.on('change', function() {
        emitWirelessList();
      });
      device.wireless.on('vanish', function() {
        emitWirelessList();
      });


      device.wireless.on('error', function(err) {
        logger.error('Wireless error: ' + err);
      });

    });
  }

  device.connectWireless = function(ssid, password, cb) {
    //var child = spawn('prince', ['-v', 'builds/pdf/book.html', '-o', 'builds/pdf/book.pdf']);
    var child = spawn('', ['-v', 'builds/pdf/book.html', '-o', 'builds/pdf/book.pdf']);

    child.stdout.on('data', function(chunk) {
      // output here
    });
  }

  device.initDevice();

  return device;
}


module.exports = buildModule;