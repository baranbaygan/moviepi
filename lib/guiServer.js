var http = require('http');
var events = require('events');
var logger = require('./logger');
var ws = require("nodejs-websocket")
var Wireless = require('wireless');
var device = require('./device')();

var HomeState = require('./guiStates/homeState');
var WirelessListState = require('./guiStates/wirelessListState');
var WirelessPasswordState = require('./guiStates/wirelessPasswordState');
var JoinWirelessState = require('./guiStates/joinWirelessState');
var BufferingState = require('./guiStates/bufferingState');
var ContentState = require('./guiStates/contentState');

function GuiServer() {

  var guiserver = new events.EventEmitter();

  var connections = [];

  var stateStack = [];

  var homeState = HomeState();
  listenStateEvents(homeState);

  var wirelessListState = WirelessListState();
  listenStateEvents(wirelessListState);

  var wirelessPasswordState = WirelessPasswordState();
  listenStateEvents(wirelessPasswordState);

  var joinWirelessState = JoinWirelessState();
  listenStateEvents(joinWirelessState);

  var bufferingState = BufferingState();
  listenStateEvents(bufferingState);

  var contentState = ContentState();
  listenStateEvents(contentState);

  pushState(homeState);


  // PUBLIC API


  guiserver.handleCommand = function(command) {
    getActiveState().onCommand(command);
  }

  guiserver.lastStateBeforePlay = null;

  guiserver.showPlayInfo = function(data) {
    console.log('guiserver.showPlayInfo');

    var activeState = getActiveState();

    if (activeState.getName() != 'buffering') {
      pushState(bufferingState);
      bufferingState.updateWithPlayData(data);
    } else {
      bufferingState.updateWithPlayData(data);
    }
  }

  guiserver.hidePlayInfo = function() {
    console.log('guiserver.hidePlayInfo');

    var activeState = getActiveState();
    if (activeState.getName() == 'buffering') {
      popState(activeState);
    }
  }

  guiserver.showContentUrl = function(url) {
    console.log('guiserver.showContentUrl');

    var activeState = getActiveState();

    // Do not show any other content while buffering screen is on
    if (activeState.getName() == 'buffering') return;

    if (activeState.getName() != 'content') {
      pushState(contentState);
      contentState.updateUrl(url);
    } else {
      contentState.updateUrl(url);
    }
  }

  // END OF PUBLIC API



  function pushState(state) {
    stateStack.push(state);
    state.onResume();

    sendToAllClients(createActiveStateMessage());
  }

  function createActiveStateMessage() {
    var state = getActiveState();
    return {
      stateName: state.getName(),
      stateData: state.getData()
    }
  }


  function popState(state) {
    var poppedState = stateStack.pop();
    poppedState.onPause();

    sendToAllClients(createActiveStateMessage());
  }

  function getActiveState() {
    return stateStack[stateStack.length - 1];
  }


  function listenStateEvents(state) {

    state.on('updated', function(data) {
      if (state == getActiveState()) sendToAllClients(createActiveStateMessage());
    });

    state.on('openState', function(data) {

      logger.debug('openState event: ' + JSON.stringify(data));

      var stateName = data.stateName;
      var stateData = data.stateData;

      var state = findStateByName(stateName);
      state.init(stateData);

      pushState(state);
    });

    state.on('finished', function() {
      if (state == getActiveState()) popState();
    });

  }

  function findStateByName(stateName) {
    switch (stateName) {
      case 'home':
        return homeState;
      case 'wirelesslist':
        return wirelessListState;
      case 'wirelesspassword':
        return wirelessPasswordState;
      case 'joinwireless':
        return joinWirelessState;
      case 'buffering':
        return bufferingState;
      case 'content':
        return contentState;
    }


    return homeState;
  }


  function sendToAllClients(data) {
    connections.forEach(function(c) {
      sendToClient(c, data);
    });
  }

  function sendToClient(c, data) {
    c.sendText(JSON.stringify(data));
  }

  function updateClients() {
    connections.forEach(function(c) {
      c.sendText(JSON.stringify(getCurrentState()));
    });
  }


  // Start socket server
  var server = ws.createServer(function(conn) {

    logger.info('GUI client connected');
    connections.push(conn);

    sendToClient(conn, createActiveStateMessage());

    conn.on("text", function(str) {
      var msg = JSON.parse(str);
    })

    conn.on("close", function(code, reason) {
      logger.info('GUI client disconnected');
      connections.splice(connections.indexOf(conn), 1);
    })


  }).listen(51911);


  return guiserver;



}


module.exports = GuiServer;