var GuiState = require('./guiState');
var logger = require('../logger');
var device = require('../device')();
var cprc = require('child_process');

function BufferingState() {

  var self = {};

  self.__proto__ = GuiState();

  self.data = {};



  self.updateWithPlayData = function(data) {
    self.data.playData = data;
    self.emitUpdated();
  }

  self.init = function(data) {

  }
  self.onResume = function() {


  }
  self.onPause = function() {

  }
  self.onCommand = function(command) {

    switch (command.code) {
      case 'green':
        {

        }
      case 'red':
        {

        }
    }
  }

  self.getName = function() {
    return 'buffering';
  }
  self.getData = function() {
    return self.data;
  }

  return self;

}

module.exports = BufferingState;