var GuiState = require('./guiState');
var logger = require('../logger');
var device = require('../device')();
var cprc = require('child_process');

function ContentState() {

  var self = {};

  self.__proto__ = GuiState();

  self.data = {

  };

  self.updateUrl = function(url) {
    self.data.url = url;
    self.emitUpdated();
  }
  self.init = function(data) {

  }
  self.onResume = function() {


  }
  self.onPause = function() {

  }
  self.onCommand = function(command) {

    if (command.name == 'exit') {
      self.finish();
      return;
    }

    switch (command.code) {
      case 'd':
      case '2a':
        {
          self.finish();
          break;
        }

    }
  }

  self.getName = function() {
    return 'content';
  }
  self.getData = function() {
    return self.data;
  }

  return self;

}

module.exports = ContentState;