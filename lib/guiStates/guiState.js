var events = require('events');

function GuiState() {

  var guiState = new events.EventEmitter();

  guiState.data = {}; // never used because getData() will be overriden

  guiState.init = function(data) {

  }
  guiState.onResume = function() {

  }
  guiState.onPause = function() {

  }
  guiState.onCommand = function(command) {

  }
  guiState.getName = function() {
    return 'gui state name';
  }
  guiState.getData = function() {
    return guiState.data;
  }


  guiState.emitUpdated = function() {
    guiState.emit('updated', guiState.getData());
  }

  guiState.openState = function(stateName, data) {
    guiState.emit('openState', {
      stateName: stateName,
      stateData: data
    });
  }

  guiState.finish = function() {
    guiState.emit('finished');
  }

  return guiState;

}

module.exports = GuiState;