var GuiState = require('./guiState');
var logger = require('../logger');
var firebaseConn = require('../movieFirebase')();
var device = require('../device')();
var cprc = require('child_process');

function HomeState() {

  var homeState = {};

  homeState.__proto__ = GuiState();

  homeState.data = {
    deviceInfo: device.getDeviceInfo()
  };

  firebaseConn.on('remotesUpdated', function(value) {
    console.log('remotesUpdated: ' + JSON.stringify(value));
    var count = 0;
    if (value) {
      count = Object.keys(value).length;
    }
    homeState.data = {
      deviceInfo: device.getDeviceInfo(),
      remotes: count
    }
    homeState.emitUpdated();
  })

  homeState.init = function(data) {

  }
  homeState.onResume = function() {


  }
  homeState.onPause = function() {

  }
  homeState.onCommand = function(command) {
    logger.debug('homeState onCommand: ' + JSON.stringify(command));

    switch (command.code) {
      case 'green':
        {
          // Wifi setup
          homeState.openState('wirelesslist', {});
          break;
        }
      case 'red':
        {
          // cprc.exec('sudo killall -q kweb3');
          // process.exit();
          // require('reboot').reboot();
          break;
        }
    }
  }

  homeState.getName = function() {
    return 'home';
  }
  homeState.getData = function() {
    return homeState.data;
  }

  return homeState;

}

module.exports = HomeState;