var GuiState = require('./guiState');
var logger = require('../logger');
var device = require('../device')();
var wirelessConnector = require('../wirelessConnector')();
var movieFirebase = require('../movieFirebase');

function JoinWirelessState() {

  var self = {};

  self.__proto__ = GuiState();

  self.data = {};
  self.data.selectedWireless = null;
  self.data.password = '';
  self.data.status = '';

  device.wireless.on('join', function() {
    logger.debug('EVENT join');
  });
  device.wireless.on('dhcp', function() {
    logger.debug('EVENT dhcp');
  });

  self.init = function(data) {
    self.data.selectedWireless = data.selectedWireless;
    self.data.password = data.password == '' ? 'ratton1020' : data.password;
  }
  self.onResume = function() {

    console.log('self.data.selectedWireless.ssid: ' + self.data.selectedWireless.ssid);
    console.log('onResume: password: ' + self.data.password);

    // device.wireless.join(self.data.selectedWireless, self.data.password, function() {
    //
    // });


    self.data.status = 'connecting ' + self.data.selectedWireless.ssid + '...';
    self.emitUpdated();
    wirelessConnector.connectWireless(self.data.selectedWireless.ssid, self.data.password, function(success) {
      if (success) {
        self.data.status = 'Connected';


        setTimeout(function() {
          //movieFirebase.ping();
          self.finish();
        }, 4000);

      } else {
        self.data.status = 'Connection failed';
      }
      self.emitUpdated();
    });

    // wirelessConnector.connectWireless('bbking', 'Ear3cubit', function(success) {
    //   console.log('success: ' + success);
    // });

  }
  self.onPause = function() {

  }
  self.onCommand = function(command) {
    logger.debug('wirelessPasswordState onCommand: ' + JSON.stringify(command));

    switch (command.code) {

      case '2a':
      case 'red':
        {
          self.finish();
          break;
        }

    }
  }


  self.getName = function() {
    return 'joinwireless';
  }
  self.getData = function() {
    return self.data;
  }

  return self;

}

module.exports = JoinWirelessState;