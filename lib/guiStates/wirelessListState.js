var GuiState = require('./guiState');
var logger = require('../logger');
var device = require('../device')();

function WirelessListState() {

  var wirelessListState = {};

  wirelessListState.__proto__ = GuiState();

  wirelessListState.wireless = null;

  wirelessListState.data = {};
  wirelessListState.data.selectedWireless = null;

  wirelessListState.init = function(data) {

  }
  wirelessListState.onResume = function() {


    device.on('wirelessList', function(data) {

      wirelessListState.data.wirelessList = data;
      if (wirelessListState.data.selectedWireless == null && data.length > 0) {
        wirelessListState.data.selectedWireless = data[0];
      }
      wirelessListState.emitUpdated();
    });
    device.scanWireless();


  }
  wirelessListState.onPause = function() {
    device.stopScanWireless();
  }
  wirelessListState.onCommand = function(command) {
    logger.debug('wirelessListState onCommand: ' + JSON.stringify(command));

    switch (command.code) {
      case '2a':
        {
          // Exit
          wirelessListState.finish();
          break;
        }
      case '1':
        {
          // Up
          if (wirelessListState.data.selectedWireless) {
            var index = indexOfObjectInArray(wirelessListState.data.selectedWireless, wirelessListState.data.wirelessList);
            if (index > 0) {
              wirelessListState.data.selectedWireless = wirelessListState.data.wirelessList[index - 1];
              wirelessListState.emitUpdated();
            }
          }

          break;
        }
      case '2':
        {
          // Down
          if (wirelessListState.data.selectedWireless) {
            var index = indexOfObjectInArray(wirelessListState.data.selectedWireless, wirelessListState.data.wirelessList);
            if (index < wirelessListState.data.wirelessList.length - 1) {
              wirelessListState.data.selectedWireless = wirelessListState.data.wirelessList[index + 1];
              wirelessListState.emitUpdated();
            }

          }
          break;
        }
      case '0':
        {
          // OK
          wirelessListState.openState('wirelesspassword', {
            selectedWireless: wirelessListState.data.selectedWireless
          });
          break;
        }
    }
  }

  wirelessListState.getName = function() {
    return 'wirelesslist';
  }
  wirelessListState.getData = function() {
    return wirelessListState.data;
  }

  return wirelessListState;

}


function indexOfObjectInArray(object, arr) {

  for (var i = 0; i < arr.length; i++) {
    if (object == arr[i]) return i;
  }
  return -1;
}

module.exports = WirelessListState;