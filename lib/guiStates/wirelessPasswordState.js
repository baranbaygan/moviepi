var GuiState = require('./guiState');
var logger = require('../logger');
var device = require('../device')();

function WirelessPasswordState() {

  var self = {};

  self.__proto__ = GuiState();

  self.data = {};
  self.data.selectedWireless = null;
  self.data.password = '';
  self.data.currentChar = ' ';
  self.data.isUpper = false;

  var chars = ['0', '1', 'abc2', 'def3', 'ghi4', 'jkl5', 'mno6', 'pqrs7', 'tuv8', 'wxyz9'];

  self.init = function(data) {
    self.data.selectedWireless = data.selectedWireless;
  }
  self.onResume = function() {

  }
  self.onPause = function() {

  }
  self.onCommand = function(command) {
    logger.debug('wirelessPasswordState onCommand: ' + JSON.stringify(command));

    switch (command.code) {
      case '2a':
        {
          // Exit
          self.finish();
          break;
        }
      case 'yellow':
        {
          self.data.isUpper = !self.data.isUpper;
          self.emitUpdated();
          break;
        }
      case 'blue':
        {
          self.data.password = 'Ear3cubit';
          self.emitUpdated();
          break;
        }
      case 'red':
        {
          self.data.password = 'ratton1020';
          self.emitUpdated();
          break;
        }
      case 'green':
        {
          // Join network
          self.openState('joinwireless', {
            selectedWireless: self.data.selectedWireless,
            password: self.data.password
          });
          break;
        }
      case '0':
        {
          if (self.data.currentChar && self.data.currentChar.length > 0) {
            self.data.password += self.data.isUpper ?
              self.data.currentChar.toUpperCase() : self.data.currentChar;
            self.data.currentChar = ' ';
            self.emitUpdated();
          }
          break;
        }
      case '3':
        {
          if (self.data.password.length > 0) {
            self.data.password = self.data.password.slice(0, -1);
            self.emitUpdated();
          }
          break;
        }
      case '20':
      case '21':
      case '22':
      case '23':
      case '24':
      case '25':
      case '26':
      case '27':
      case '28':
      case '29':
        {
          handleCharEntry(parseInt(command.code) - 20);
          console.log('self.data.currentChar: ' + self.data.currentChar);
          self.emitUpdated();
          break;
        }

    }
  }

  function handleCharEntry(key) {
    console.log('handleCharEntry: ' + key);
    var set = chars[key];
    var i = -1;

    i = set.indexOf(self.data.currentChar ? self.data.currentChar.toLowerCase() : self.data.currentChar) + 1;


    console.log('i: ' + i + ' set length: ' + set.length + ' char: ' + self.data.currentChar);

    if (i > set.length) i--;
    self.data.currentChar = set[i];
    if (self.data.isUpper && self.data.currentChar) {
      self.data.currentChar = self.data.currentChar.toUpperCase();
    }
  }

  self.getName = function() {
    return 'wirelesspassword';
  }
  self.getData = function() {
    return self.data;
  }

  return self;

}


module.exports = WirelessPasswordState;