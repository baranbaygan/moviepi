var colors = require('colors');
var fs = require('fs');

var winston = require('winston');

winston.add(winston.transports.File, {
  filename: 'moviepi.log',
  level: 'silly'
});

var logger = {};

// Special Debug Console Calls!
logger.log = console.log.bind(console);
logger.debug = function() {
  winston.log('debug', arguments[0]);
  //console.log(colors.cyan('[DEBUG]: ') + arguments[0]);
};
logger.info = function() {
  winston.log('info', arguments[0]);
  //console.log(colors.green('[INFO]: ') + arguments[0]);
};
logger.warn = function() {
  winston.log('warn', arguments[0]);
  //console.log(colors.yellow('[WARNING]: ') + arguments[0]);
};
logger.error = function() {
  winston.log('error', arguments[0]);
  //console.log(colors.red('[ERROR]: ') + arguments[0]);
};

module.exports = logger;