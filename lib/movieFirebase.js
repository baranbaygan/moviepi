var Firebase = require('firebase');
var device = require('./device')();
var events = require('events');
var logger = require('./logger');
var os = require('os');
var wc = require('./wirelessConnector')();
var _ = require('underscore');

var defaultFirebaseUrl = 'https://moviepi.firebaseio.com';
var deviceInfo = device.getDeviceInfo();
var ifaces = os.networkInterfaces();

var m = null;

function buildModule(opts) {



  if (m) return m;

  if (!opts) opts = {};
  if (!opts.firebaseUrl) opts.firebaseUrl = defaultFirebaseUrl;

  var firebaseModule = new events.EventEmitter();
  firebaseModule.isConnected = false;

  m = firebaseModule;

  require('getmac').getMac(function(err, macAddress) {
    if (err) throw err
    var deviceRef = new Firebase(opts.firebaseUrl + '/device/' + macAddress);
    deviceRef.once('value', function(snap) {
      if (snap && snap.val()) {
        deviceRef.child('lastSeen').set(Firebase.ServerValue.TIMESTAMP);
        deviceRef.child('commandId').set(deviceInfo.id);
      } else {
        deviceRef.set({
          install: Firebase.ServerValue.TIMESTAMP,
          lastSeen: Firebase.ServerValue.TIMESTAMP,
          commandId: deviceInfo.id
        })
      }
    });
  })

  var remotesRef = new Firebase(opts.firebaseUrl + '/remote/' + deviceInfo.id);
  remotesRef.on('value', function(snap) {
    if (snap && snap.val()) {

      firebaseModule.emit('remotesUpdated', snap.val());
    } else {
      firebaseModule.emit('remotesUpdated', null);
    }
  })

  var nowPlayingRef = new Firebase(opts.firebaseUrl + '/nowplaying/' + deviceInfo.id);
  nowPlayingRef.onDisconnect().set(null);

  var nowPlayingMediaRef = new Firebase(opts.firebaseUrl + '/nowplayingmedia/' + deviceInfo.id);
  nowPlayingMediaRef.onDisconnect().set(null);

  var pingRef = new Firebase(opts.firebaseUrl + '/ping/' + deviceInfo.id);
  pingRef.onDisconnect().set(null);

  var statusRef = new Firebase(opts.firebaseUrl + '/deviceStatus/' + deviceInfo.id);
  statusRef.set('idle');
  statusRef.onDisconnect().set(null);



  // APP Store related
  var appStoreRef = new Firebase(opts.firebaseUrl + '/appstore');
  var myAppsRef = new Firebase(opts.firebaseUrl + '/apps/' + deviceInfo.id);

  function updateApps() {
    appStoreRef.once('value', function(snap) {
      if (snap && snap.val()) {
        var storeApps = snap.val();
        myAppsRef.once('value', function(myAppsSnap) {

          var firstInstall = false;
          if (myAppsSnap && myAppsSnap.val()) {
            var myAppsObj = myAppsSnap.val();
            var myApps = myAppsObj.installed;
          } else {
            firstInstall = true;
          }

          if (!myApps) myApps = {};


          _.each(storeApps, function(app) {

            var isInstalled = _.where(myApps, {
              url: app.url
            }).length > 0;

            if (firstInstall) {
              if (app.default)
                myApps[app.id] = app;
            } else if (!isInstalled && app.force) {
              myApps[app.id] = app;
            }
          });

          myAppsRef.set({
            me: deviceInfo.id,
            installed: myApps
          });


        });
      }
    });
  }
  updateApps();
  setInterval(function() {
    updateApps();
  }, 10000);
  // End of App Store related



  var commandRef = new Firebase(opts.firebaseUrl + '/command/' + deviceInfo.id);
  commandRef.set('');
  commandRef.onDisconnect().set(null);
  commandRef.on('value', function(snap) {
    if (snap.val() && snap.val() != '') {

      firebaseModule.emit('command', snap.val());

      commandRef.set('');
    }
  });

  var tvCommandRef = new Firebase(opts.firebaseUrl + '/tvCommand/' + deviceInfo.id);
  tvCommandRef.on('value', function(snap) {
    if (snap.val() && snap.val() != '') {
      firebaseModule.emit('tvCommand', snap.val());
      tvCommandRef.set('');
    }
  });

  var guiStateRef = new Firebase(opts.firebaseUrl + '/guiState/' + deviceInfo.id);
  guiStateRef.set({
    name: 'start',
    data: null
  });
  guiStateRef.on('value', function(snap) {
    if (snap.val() && snap.val() != '') {
      firebaseModule.emit('guiStateChange', snap.val());
    }
  });



  require('getmac').getMac(function(err, macAddress) {
    var onlineRef = new Firebase(opts.firebaseUrl + '/online/' + macAddress);
    onlineRef.set({
      mac: macAddress
    });
    onlineRef.onDisconnect().set(null);
  });



  var amOnline = new Firebase(opts.firebaseUrl + '/.info/connected');
  amOnline.on('value', function(snapshot) {
    logger.info('snapshot ' + snapshot.val());
    if (snapshot.val()) {
      // User is online.
      firebaseModule.isConnected = true;
      firebaseModule.emit('connected');
    } else {
      // User is offline.
      firebaseModule.isConnected = false;
      firebaseModule.emit('disconnected');
    }
  });

  wc.on('connected', function() {
    console.log('Connection changed. (Firebase refresh)');
    Firebase.goOffline();
    Firebase.goOnline();
  })

  // END OF Firebase Related

  // Get network addresses and write them to Firebase
  Object.keys(ifaces).forEach(function(ifname) {
    var alias = 0;

    ifaces[ifname].forEach(function(iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }

      if (alias >= 1) {
        // this single interface has multiple ipv4 addresses

      } else {
        // this interface has only one ipv4 adress
        //console.log(ifname, iface.address);

        var fbRef = new Firebase(opts.firebaseUrl + '/' + ifname + '/' + deviceInfo.id);
        fbRef.onDisconnect().set(null);
        fbRef.set(iface.address);
      }
    });
  });



  // Public methods
  firebaseModule.updateStatus = function(status) {
    statusRef.set(status);
  }
  firebaseModule.updateNowPlaying = function(data) {
    nowPlayingRef.set(data);
  }
  firebaseModule.updateNowPlayingMedia = function(data) {
    nowPlayingMediaRef.set(data);
  }

  firebaseModule.ping = function() {
    pingRef.set(Firebase.ServerValue.TIMESTAMP);
  }

  setInterval(function() {
    firebaseModule.ping();
  }, 5000);

  return firebaseModule;

}


module.exports = buildModule;