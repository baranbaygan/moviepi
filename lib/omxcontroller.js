var events = require('events');
var logger = require('./logger');
var cp = require('child_process');
var exec = cp.exec;
var psTree = require('ps-tree');

var omxprocess = null;
var omxdbusScript = './omxdbus.sh';

function createModule() {

  var omxcontroller = new events.EventEmitter();

  var playerUpdateInterval = 1000;
  var isPaused = false;

  var isPlaying = false;

  omxcontroller.media = null;

  omxcontroller.playTorrent = function(media, port) {


    if (media.subtitlePath) {
      omxcontroller.media = media;
      omxprocess = exec('omxplayer --subtitles ' + media.subtitlePath + ' --timeout 30000 http://localhost:' + port);
    } else {
      omxprocess = exec('omxplayer --timeout 30000 http://localhost:' + port);
    }

    isPlaying = true;

    omxprocess.on('exit', function() {
      isPlaying = false;
      omxcontroller.emit('stopped');
    })



  }

  omxcontroller.playYoutube = function(media) {

    logger.info('omxprocess: ' + omxprocess + ' command: ' + 'omxplayer -o hdmi $(/usr/local/bin/youtube-dl -g "' + media.url + '")');


    omxcontroller.media = media;
    omxprocess = exec('omxplayer -o hdmi $(/usr/local/bin/youtube-dl -g "' + media.url + '" --user-agent "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36")');

    isPlaying = true;

    omxprocess.on('exit', function() {
      isPlaying = false;
      omxcontroller.emit('stopped');
    })

    logger.info('omxprocess: ' + omxprocess);


  }

  omxcontroller.stop = function(cb) {

    logger.info('omxcontroler Stop called ' + omxprocess);

    //exec('sudo killall omxplayer.bin');
    if (omxprocess) {
      psTree(omxprocess.pid, function(err, children) {
        cp.spawn('kill', ['-9'].concat(children.map(function(p) {
          return p.PID
        })))
      })
    }


    setTimeout(function() {
      if (cb) cb();
    }, 2000);



    // if (omxprocess) {
    //   kill(omxprocess.pid, null, function() {
    //     omxprocess = null;
    //     if (cb) cb();
    //   });
    // } else {
    //   if (cb) cb();
    // }
  }

  omxcontroller.play = function() {
    if (isPaused)
      exec(omxdbusScript + ' pause');
  }

  omxcontroller.pause = function() {
    exec(omxdbusScript + ' pause');
  }

  omxcontroller.volumeup = function() {
    exec(omxdbusScript + ' volumeup');
  }

  omxcontroller.volumedown = function() {
    exec(omxdbusScript + ' volumedown');
  }

  omxcontroller.seek = function(offset) {
    exec(omxdbusScript + ' seek ' + offset);
  }


  function getOmxPlayerStatus() {
    if (omxprocess && isPlaying) {
      exec(omxdbusScript + ' status', function(error, stdout, stderr) {
        // if (error) logger.error(error);
        // if (stdout) logger.info(stdout);
        // if (stderr) logger.error(stderr);

        if (!error && !stderr) {
          var items = stdout.split('|');
          omxcontroller.emit('status', {
            'duration': items[0].split(':')[1],
            'position': items[1].split(':')[1],
            'paused': items[2].split(':')[1],
            'volume': items[3].split(':')[1]
              //'media': omxcontroller.media ? omxcontroller.media : null
          });
          isPaused = items[2].split(':')[1];
        } else {
          logger.error('omx script error ' + error);
          omxcontroller.emit('status', null);
        }
        setTimeout(getOmxPlayerStatus, playerUpdateInterval);
      });
    } else {
      setTimeout(getOmxPlayerStatus, playerUpdateInterval);
    }
  }
  getOmxPlayerStatus();




  return omxcontroller;
}

module.exports = createModule;



// Utils

var kill = function(pid, signal, callback) {
  signal = signal || 'SIGKILL';
  callback = callback || function() {};
  var killTree = true;
  if (killTree) {
    psTree(pid, function(err, children) {
      [pid].concat(
        children.map(function(p) {
          return p.PID;
        })
      ).forEach(function(tpid) {
        try {
          process.kill(tpid, signal)
        } catch (ex) {}
      });
      callback();
    });
  } else {
    try {
      process.kill(pid, signal)
    } catch (ex) {}
    callback();
  }
};