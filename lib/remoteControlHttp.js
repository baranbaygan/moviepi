var http = require('http');
var os = require('os');
var ifaces = os.networkInterfaces();

var remoteHttpPort = 1919;

function remoteControlHttp(opts) {

  var remote = {};

  var server = http.createServer(function(req, res) {
    res.writeHead(200, {
      'Content-Type': 'text/plain'
    });

    res.end('REMOTE OK');
  });





  server.listen(remoteHttpPort);

  return remote;

}


module.exports = remoteControlHttp;