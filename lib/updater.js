var events = require('events');
var logger = require('./logger')
var cprc = require('child_process');
var request = require('request');
var pjson = require('../package.json');
var compareVersion = require('compare-version');

function build() {

  var self = new events.EventEmitter();

  self.updateAvailable = false;

  console.log(pjson.version);

  self.startChecking = function() {
    setInterval(function() {

      logger.info('Updater checking for updates');

      // cprc.exec("git pull", function(error, stdout, stderr) {
      //
      // });


      request('https://moviepi.firebaseio.com/appVersion.json', function(error, response, body) {
        //request('https://bitbucket.org/baranbaygan/moviepi/raw/master/package.json', function(error, response, body) {
        if (body) {
          var packageJson = JSON.parse(body);

          console.log('Current: ' + pjson.version + ' cloud: ' + packageJson.version);

          var versionComparison = compareVersion(pjson.version, packageJson.version)
          if (versionComparison == -1) {
            logger.info('New version available');
          } else {
            logger.info('Up to date');
          }


        }
      });

    }, 3000);
  }

  return self;
}

module.exports = build();