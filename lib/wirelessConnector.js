var http = require('http');
var spawn = require('child_process').spawn;
var cprc = require('child_process');
var events = require('events');
var Firebase = require('firebase');

var self = null;

function WirelessConnector() {

  if (self) return self;

  var self = new events.EventEmitter();

  self.isCancelled = false;
  self.timer = null;

  function checkConnectivity(cb) {
    var connected = false;
    var tryCount = 20;
    self.timer = setInterval(function() {

      console.log('connection try tryCount: ' + tryCount);

      cprc.exec('sudo ifconfig wlan0 | grep inet', function(error, stdout, stderr) {
        console.log('connection try: stdout: ' + stdout);

        if (stdout && stdout != '') {
          connected = true;
        }
      });

      if (self.isCancelled || connected || tryCount-- == 0) {
        clearInterval(self.timer);
        cb(connected);
      }

    }, 1000);

  }

  self.cancel = function() {
    self.isCancelled = true;
    if (self.timer) clearInterval(self.timer);
  }
  self.connectWireless = function(ssid, pass, cb) {

    // Create conf file command
    var cmd = 'wpa_passphrase "' + ssid + '" ' + pass + ' > /etc/wpa_supplicant/wpa_supplicant.conf';

    console.log('connecting command: ' + cmd);

    //var createPassChild = spawn('wpa_passphrase ' + ssid + ' ' + pass, [ssid, pass]);

    cprc.exec(cmd, function(error, stdout, stderr) {

      if (error) {
        cb(false);
        console.error(stdout + ' ' + error);
      }



      cprc.exec('sudo killall -q wpa_supplicant', function() {

        cprc.exec('sudo ifdown wlan0', function(error, stdout, stderr) {

          var child = cprc.exec('sudo ifup wlan0', {
            timeout: 15000
          }, function(error, stdout, stderr) {

            if (stdout)
              console.log('ifup stdout: ' + stdout.toString());
            if (error)
              console.log('ifup error: ' + error.toString());
            if (stderr)
              console.log('ifup stderr: ' + stderr.toString());

            var success = false;

            if (stdout && stdout.indexOf('bound to') != -1) success = true;
            if (stderr && stderr.indexOf('bound to') != -1) success = true;

            if (success) {
              cb(true);

              Firebase.goOffline();
              Firebase.goOnline();
              console.log('Firebase goOffline goOnline 1');
            } else {



              console.log('Second connection try');


              cprc.exec('sudo ifdown wlan0', function(error, stdout, stderr) {

                var child = cprc.exec('sudo ifup wlan0', {
                  timeout: 15000
                }, function(error, stdout, stderr) {

                  if (stdout)
                    console.log('ifup2 stdout: ' + stdout.toString());
                  if (error)
                    console.log('ifup2 error: ' + error.toString());
                  if (stderr)
                    console.log('ifup2 stderr: ' + stderr.toString());

                  var success = false;

                  if (stdout && stdout.indexOf('bound to') != -1) success = true;
                  if (stderr && stderr.indexOf('bound to') != -1) success = true;

                  cb(success);

                  if (success) {
                    Firebase.goOffline();
                    Firebase.goOnline();
                    console.log('Firebase goOffline goOnline 2');
                  }



                })
              });








            }




          });

        });
      });

      // cprc.exec('sudo killall -q wpa_supplicant', function(error, stdout, stderr) {
      //
      //   if (error) {
      //     //cb(false);
      //     console.error(stdout + ' ' + error);
      //   }
      //
      //   //wpa_supplicant -B w -D wext -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf
      //   cprc.exec('sudo wpa_supplicant -B w -D wext -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf', function(error, stdout, stderr) {
      //     if (error) {
      //       cb(false);
      //       console.error(stdout + ' ' + error);
      //     }
      //
      //     checkConnectivity(function(success) {
      //       cb(success);
      //     });
      //   });
      //
      //
      // });

    });


  }

  return self;
}

module.exports = WirelessConnector;