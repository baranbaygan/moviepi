var logger = require('./lib/logger');
var device = require('./lib/device')();
var remoteControlHttp = require('./lib/remoteControlHttp')();
var movieFirebase = require('./lib/movieFirebase');
var events = require('events');
var peerflix = require('peerflix');
var Firebase = require('firebase');
var rmdir = require('rimraf');
var del = require('del');
var omxcontroller = require('./lib/omxcontroller')();
var exec = require('child_process').exec;
var fs = require('fs');
var https = require('https');
var http = require('http');
var Cecclient = require('nodecec');
var logger = require('./lib/logger');
var guiServer = require('./lib/guiServer')();
var domain = require('domain');
var checkOnline = require('./lib/checkOnline');
var path = require('path')
var AdmZip = require('adm-zip');
var mkdirp = require('mkdirp');
var utf8 = require('utf8');
var rmdir = require('./lib/rmdir');
var Iconv = require('iconv').Iconv;
var updater = require('./lib/updater');

// Default firebase url
var defaultFirebaseUrl = 'https://moviepi.firebaseio.com';
var torrentTmpFolder = '/tmp/torrent-stream';
var subtitleFolder = '/tmp/subtitles';
var deviceInfo = device.getDeviceInfo();
var omxprocess = null;
var streamServerPort = 57916;
var isOnline = false;


function moviePi(opts) {

  // check options
  if (!opts) opts = {};
  if (!opts.firebaseUrl) opts.firebaseUrl = defaultFirebaseUrl;





  process.on('uncaughtException', function(err) {
    logger.error(err);
    logger.error("Node NOT Exiting...");
    console.log(err);
  });





  var moviePiEngine = new events.EventEmitter();
  moviePiEngine.status = 'idle';
  moviePiEngine.isConnected = false;
  moviePiEngine.torrentEngine = null;


  moviePiEngine.updater = updater;
  moviePiEngine.updater.startChecking();


  checkOnline(function(ison) {
    isOnline = ison;
  })

  var cecclient = null;

  function connectCecClient() {
    cecclient = new Cecclient();
    cecclient.start();
    cecclient.on('error', function(data) {
      logger.error('cecclient error: ' + data);
      if (data.toString().indexOf('ENOENT') != -1) {
        cecclient.stop(function() {
          setTimeout(function() {
            connectCecClient();
          }, 5000);
        });
      }
    });
    cecclient.on('key', function(key) {
      executeCecCommand(key);
    });
  }
  connectCecClient();




  // TEST CEC Command Server
  http.createServer(function(req, res) {

    var url = require('url');
    var url_parts = url.parse(req.url);
    var query = url_parts.query;
    var cec_command = {
      code: query.split('=')[1],
      name: ''
    };
    executeCecCommand(cec_command);
    res.end();
  }).listen(1212);



  logger.info('Connecting...');

  var firebaseConn = movieFirebase();

  firebaseConn.on('command', function(command) {
    logger.info('Command received: ' + command);

    executeCommand(command);
  });

  firebaseConn.on('connected', function() {
    logger.info('Connected');

    logger.info('Remote control ID: ' + deviceInfo.id);
  });

  firebaseConn.on('disconnected', function() {
    logger.info('Disconnected');
  });

  firebaseConn.on('play', function(media) {
    logger.info('Play requested for media: ' + media.url);

    stopPlaying(function() {
      startPlaying(media);
    });

  });

  firebaseConn.on('tvCommand', function(data) {
    logger.info('TV Command received: ' + data);
    wakeUp();
    switch (data.command) {
      case 'openPageTV':
        {
          guiServer.showContentUrl(data.url);
          break;
        }
      case 'play':
        {
          startPlaying(data);
          break;
        }
      case 'playPause':
        {
          if (omxcontroller) omxcontroller.play();
          break;
        }
      case 'stop':
        {
          stopPlaying(function() {

          });

          break;
        }
      case 'seek':
        {
          if (data.offset) {
            omxcontroller.seek(parseInt(data.offset) * 1000000);
          }
          break;
        }

    }

  });

  firebaseConn.on('stop', function() {
    logger.info('Stop playing');

    stopPlaying(function() {

    });
  });


  omxcontroller.on('stopped', function() {
    console.log('omxcontroller on stopperd');
    guiServer.hidePlayInfo();
  });
  omxcontroller.on('status', function(data) {
    firebaseConn.updateNowPlaying(data);
  })




  // Private methods

  function executeCecCommand(command) {

    logger.info('CEC Key: ' + JSON.stringify(command));
    wakeUp();
    guiServer.handleCommand(command);

    var code = command.code + '';
    var name = command.name + '';
    switch (code) {
      case '41':
      case '44':
        {
          omxcontroller.play();
          break;
        }
      case '46':
        {
          omxcontroller.pause();
          break;
        }
      case '42':
      case '45':
        {
          stopPlaying();

          //omxcontroller.stop();
          break;
        }
      case '48':
        {
          omxcontroller.seek(-30000000);
          break;
        }
      case '49':
        {
          omxcontroller.seek(30000000);
          break;
        }
      case 'fbrc':
        {
          console.log('FIREBASE RECONNECT');
          Firebase.goOffline();
          Firebase.goOnline();
          break;
        }
    }
  }

  function executeCommand(command) {
    wakeUp();
    guiServer.handleCommand(command);

    var cmd = command.split('|')[0];
    var param1 = command.split('|')[1];

    switch (cmd) {
      case 'p':
        {
          omxcontroller.pause();
          break;
        }
      case '+':
        {
          omxcontroller.volumeup();
          break;
        }
      case '-':
        {
          omxcontroller.volumedown();
          break;
        }
      case 'seek':
        {
          omxcontroller.seek(param1);
          break;
        }

    }
  }



  function startPlaying(media) {

    logger.info('startPlaying: ' + JSON.stringify(media));

    // First stop if there is something playing
    stopPlaying(function() {

      // Nothing is playin now. So clear the cache from previous sessions
      clearCache(function(err) {

        if (err) {
          logger.error('clear cache failed: ' + err);
        }

        guiServer.showPlayInfo({
          playingStatus: 'downloading subtitle'
        });

        firebaseConn.updateNowPlayingMedia(media);

        downloadSubtitle(media, function(subtitlePath) {
          if (subtitlePath != '') {
            logger.info('Subtitle downloaded');
            media.subtitlePath = subtitlePath;
          }

          switch (media.mediaType) {
            case 'magnet':
              {
                startTorrentPlay(media);
                break;
              }
            case 'youtube':
              {
                startYoutubePlay(media);
                break;
              }
          }
        });


      });


    });



  }


  function downloadSubtitle(media, cb) {

      //var subtitlePath = '/Users/baranbaygan/Sites/subtitles/';
      var subtitlePath = '/tmp/'



      if (media.subtitleUrl) {


        mkdirp(subtitlePath + 'subs', function(err) {
          if (err) console.error(err)
          else console.log('pow!')
        });

        // rm previous subtitles folder
        rmdir('/tmp/subtitles', function() {});

        guiServer.showPlayInfo({
          playingStatus: 'downloading subtitle'
        });

        var ext = path.extname(media.subtitleUrl);

        logger.info('Downloading subtitle');

        var file = fs.createWriteStream(subtitlePath + "subtitle" + ext);
        var protocol = media.subtitleUrl.indexOf('https') == 0 ? https : http;
        var request = protocol.get(media.subtitleUrl, function(response) {

          if (ext == '.zip') {
            // Unzip subtitle archive

            response.pipe(file);

            setTimeout(function() {
              var zip = new AdmZip(subtitlePath + "subtitle.zip");
              var zipEntries = zip.getEntries(); // an array of ZipEntry records

              var StringDecoder = require('string_decoder').StringDecoder;
              var decoder = new StringDecoder('utf8');

              for (var i in zipEntries) {

                var entry = zipEntries[i];

                if (entry.entryName.endsWith('srt')) {

                  logger.info('entry: ' + entry.entryName);

                  var buffer = null;

                  var decoded = decoder.write(entry.getData())
                  if (decoded.indexOf('�') == -1) {

                    logger.info('Subtitle is UTF8');
                    // This is utf8 most probably
                    buffer = entry.getData();

                  } else {

                    logger.info('Subtitle is NOT UTF8');

                    var convertError = null;
                    var i = 0;
                    var possibleEncodings = ['CP1254', 'ISO-8859-9']

                    while (buffer == null && possibleEncodings.length > i) {
                      try {
                        var enc = possibleEncodings[i++];
                        logger.info('converting subtitle from: ' + enc + ' to UTF-8');
                        var iconv = new Iconv(possibleEncodings[i], 'UTF-8');
                        buffer = iconv.convert(entry.getData());
                      } catch (err) {
                        logger.info('error: ' + err);
                      }
                    }

                  }

                  //console.log('decoded: ' + decoded);

                  if (buffer == null) buffer = entry.getData();

                  if (buffer) {
                    logger.info('buffer exists');
                    var wstream = fs.createWriteStream(subtitlePath + 'subs/subtitle.srt');
                    wstream.write(buffer);

                  } else {
                    logger.error('buffer null');
                  }



                }
              }
              //zip.extractAllTo('/tmp/subtitles/', true);

              if (cb) cb('/tmp/subs/*.srt');
            }, 1000);

          } else {
            response.setEncoding('utf8');
            response.pipe(file);
            if (cb) cb(subtitlePath + 'subtitle' + ext);
          }

        });

      } else {
        if (cb) cb('');
      }
    }
    // downloadSubtitle({
    //   //subtitleUrl: 'http://www.yifysubtitles.com/subtitle-api/paddington-yify-43477.zip'
    //   //subtitleUrl: 'http://www.yifysubtitles.com/subtitle-api/lucy-yify-30499.zip'
    //   //subtitleUrl: 'http://www.yifysubtitles.com/subtitle-api/big-hero-6-yify-36547.zip'
    //   subtitleUrl: 'http://www.yifysubtitles.com/subtitle-api/fury-yify-34659.zip'
    //
    // })

  var speedInterval;

  function startTorrentPlay(media) {

    logger.info('Torrent play requested for media: ' + media.name);

    // First stop playing if there is any playing in progress
    guiServer.showPlayInfo({
      playingStatus: 'streaming torrent'
    });

    updateStatus('buffering');
    moviePiEngine.torrentEngine = peerflix(media.url, {
      port: streamServerPort
    });

    moviePiEngine.torrentEngine.on('ready', function() {
      updateStatus('playing');

      clearInterval(speedInterval);
      speedInterval = setInterval(function() {
        if (moviePiEngine.torrentEngine.swarm && moviePiEngine.torrentEngine.swarm.wires) {
          var speed = 0;
          var wireCount = 0;
          for (var i in moviePiEngine.torrentEngine.swarm.wires) {
            var wire = moviePiEngine.torrentEngine.swarm.wires[i];
            speed += wire.downloadSpeed();
            wireCount++;
          }
          guiServer.showPlayInfo({
            playingStatus: 'streaming torrent',
            speed: autoFormatFilesize(speed) + '/sec',
            wires: wireCount + ' peers'
          });
          console.log('speed: ' + speed);
        }
      }, 1000);

      omxcontroller.playTorrent(media, streamServerPort);
    });

  }

  function startYoutubePlay(media) {


    guiServer.showPlayInfo({
      playingStatus: 'streaming youtube'
    });

    updateStatus('starting youtube play: ' + media.url);
    omxcontroller.playYoutube(media);

  }

  function stopPlaying(cb) {

    logger.info('stopPlaying called');

    updateStatus('idle');

    firebaseConn.updateNowPlayingMedia(null);

    if (omxcontroller) {

      omxcontroller.stop(function() {

        guiServer.hidePlayInfo();
        clearInterval(speedInterval);


        try {
          // Stop and clean peerflix or other streams
          if (moviePiEngine.torrentEngine) {
            moviePiEngine.torrentEngine.destroy(function() {
              // Stop peerflix stream http server
              if (moviePiEngine.torrentEngine.server) moviePiEngine.torrentEngine.server.close();
              if (cb) cb();
            });
          } else {
            if (cb) cb();
          }
        } catch (err) {
          if (cb) cb();
        }

      });

    } else {

      guiServer.hidePlayInfo();
      clearInterval(speedInterval);
      if (cb) cb();
    }

  }

  // function killPlayer() {
  //   if (omxprocess) kill(omxprocess.pid);
  // }


  function updateStatus(status) {
      logger.info('Status: ' + status);
      moviePiEngine.status = status;
      firebaseConn.updateStatus(status);
    }
    // End of private methods

  function wakeUp() {
    // exec('export DISPLAY=:0', function() {
    //   exec('xset s activate', function() {
    //
    //   });
    // });

  }

  function autoFormatFilesize(size) {
    if (size > 1000000000) {
      return (size / 1000000000.0)
        .toPrecision(3) + " GB";
    } else if (size > 1000000) {
      return (size / 1000000.0)
        .toPrecision(3) + " MB";
    } else if (size > 1000) {
      return (size / 1000.0)
        .toPrecision(3) + " KB";
    } else {
      return size + " B"
    }
  }

  function clearCache(cb) {
    logger.info('Clearing cache');


    rmdir('/tmp/subtitles');
    rmdir('/tmp/torrent-stream', function(err) {
      if (err) logger.error('cache clear error: ' + err);
      else logger.info('cache cleared');

      if (cb) cb(err);
    });

  }

  clearCache(function(err) {
    console.log('clearCache result: ' + err);
  });

  // console.log('removing temp folder');
  // rmdir('/tmp', function(error) {
  //   console.log('removing temp folder error: ' + error);
  // });

  return moviePiEngine;

}



module.exports = moviePi;