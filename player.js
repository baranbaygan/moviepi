var Firebase = require('firebase');
var device = require('./device');
var consts = require('./constants');
var logger = require('./logger');
var peerflix = require('peerflix');
var util = require("util");
var events = require("events");

function translateCommand(command) {

}

function Player() {

  events.EventEmitter.call(this);

  var currentStatus;
  var torrentEngine;


  var commandRef = nowPlayingRef.child('command');
  commandRef.on('value', function(snap) {
    if (snap.val() && snap.val() != '') {
      logger.info('Player received command: ' + snap.val());
      commandRef.set('');
    }
  });

  var mediaRef = nowPlayingRef.child('media');
  mediaRef.on('value', function(snap) {

  });

  function updateStatus(status) {
    currentStatus = status;
    nowPlayingRef.child('status').set(status);

    logger.info('Player: ' + status);
  }

  function startTorrentPlay(mediaInfo) {

  }

  function startYoutubePlay(mediaInfo) {

  }

  function stopAndClearPlayer() {
    if (torrentEngine) {
      torrentEngine.destroy(function() {
        torrentEngine.remove();
      })

    }
  }

  updateStatus('idle');
}


util.inherits(Player, events.EventEmitter);

var player = new Player();

module.exports = player;
