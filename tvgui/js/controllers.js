tvgui.controller('MainController', ['$scope', '$rootScope', function($scope, $rootScope) {

  $rootScope.socket = new WebSocket("ws://localhost:51911");
  //$rootScope.socket = new WebSocket("ws://192.168.1.103:51911");
  $rootScope.socket.onopen = function(event) {};

  $rootScope.socket.onmessage = function(event) {

    var incoming = angular.fromJson(event.data);
    console.log('incoming: ' + JSON.stringify(incoming));

    $rootScope.$apply(function() {
      $rootScope.state = incoming.stateName;
      $rootScope.$broadcast(incoming.stateName + ':data', incoming.stateData);
    });

  }

}]);



tvgui.controller('HomeController', ['$scope', '$rootScope', function($scope, $rootScope) {

  $rootScope.$on('home:data', function(event, data) {
    console.log('home data: ' + JSON.stringify(data));

    $scope.data = data;
  })


}]);

tvgui.controller('WirelessListController', ['$scope', '$rootScope', function($scope, $rootScope) {

  $rootScope.$on('wirelesslist:data', function(event, data) {
    console.log('wirelesslist data: ' + JSON.stringify(data));

    $scope.data = data;
  })

}]);

tvgui.controller('WirelessPasswordController', ['$scope', '$rootScope', function($scope, $rootScope) {

  $rootScope.$on('wirelesspassword:data', function(event, data) {
    console.log('wirelesspassword data: ' + JSON.stringify(data));

    $scope.data = data;
  })

}]);

tvgui.controller('JoinWirelessController', ['$scope', '$rootScope', function($scope, $rootScope) {

  $rootScope.$on('joinwireless:data', function(event, data) {
    console.log('joinwireless data: ' + JSON.stringify(data));

    $scope.data = data;
  })

}]);

tvgui.controller('BufferingController', ['$scope', '$rootScope', function($scope, $rootScope) {

  $rootScope.$on('buffering:data', function(event, data) {
    console.log('buffering data: ' + JSON.stringify(data));

    $scope.data = data;
  })

}]);

tvgui.controller('ContentController', ['$scope', '$rootScope', '$sce', function($scope, $rootScope, $sce) {

  $rootScope.$on('content:data', function(event, data) {
    console.log('content data: ' + JSON.stringify(data));
    $scope.url = $sce.trustAsResourceUrl(data.url);
  })

}]);
