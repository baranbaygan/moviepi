var keypress = require('keypress');
var wc = require('./lib/wirelessConnector')();

var state = 'ssid';
var ssid = '';
var password = '';

// make `process.stdin` begin emitting "keypress" events
keypress(process.stdin);
console.log('ssid:')

// listen for the "keypress" event
process.stdin.on('keypress', function(ch, key) {

  //console.log('ch: ' + ch);

  if (ch && !key) {
    key = {
      name: ch
    }
  }

  //console.log('got "keypress"', key);
  if (key && key.ctrl && key.name == 'c') {
    process.stdin.pause();
  }

  if (key.name == 'return') {



    if (state == 'ssid') {
      state = 'password'
      console.log('enter password:');
    } else if (state == 'password') {

      state = 'connecting';
      console.log('connecting ' + ssid);

      wc.connectWireless(ssid, password, function(success) {
        if (success) {
          console.log('Connected');
        } else {
          console.log('Failed');
        }
      });

    }

  } else if (key.name == 'escape') {



    if (state == 'connecting') {

      console.log('cancelling');


      // TODO cancel wireless
      state = 'ssid';
      ssid = '';
      password = '';
      console.log('ssid:');
    }


  } else {

    if (state == 'ssid') {
      ssid += ch;
      console.log('ssid: ' + ssid);
    } else {
      password += ch;
      console.log('password: ' + password);
    }
  }


});

process.stdin.setRawMode(true);
process.stdin.resume();